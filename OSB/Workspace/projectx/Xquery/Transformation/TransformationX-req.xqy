xquery version "1.0" encoding "Utf-8";

(: _input_schema=SchemaX :)
(: _output_schema= :)
(: _input_element=input :)
(: _output_element= :)
(: _is_complex=false :)
(: _type=request :)
(: _shared_data=:)
(: _referenced_keys=:)

declare namespace xf = "urn:aorta:transformation_x-req";



declare function xf:TransformationX-req($request as element(*))
    as element(*) {
        
            let $payload := $request//*:Payload
            return

		<AdapterMessage>
		  <payload>{data($payload)}</payload>
		  <httpauth/>
		  <soapheader/>
		</AdapterMessage>
};

declare variable $request as element(*) external;


xf:TransformationX-req($request)
